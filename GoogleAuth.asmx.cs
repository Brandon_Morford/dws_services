﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Services;
using System.Xml.Linq;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Requests;
using Google.Apis.Oauth2.v2;

namespace WdhServices
{
    /// <summary>
    /// Summary description for GoogleAuth
    /// </summary>
    [WebService(Namespace = "https://dws.wyo.gov/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    
    public class GoogleAuth : WebService
    {
        //private static readonly ILog log = LogManager.GetLogger(typeof(Error));
        const string client_id = "596214849598-rrplqbk3cbncvii6abhcg3hqtpv0bkbr.apps.googleusercontent.com";
        const string client_secret = "hfqwr1a23rrTmYW2KEgrY7kG";
        static readonly string[] scopes = { Oauth2Service.Scope.UserinfoEmail };
        public GoogleAuth()
        {
        }

        [WebMethod]
        public string GetAuthUrl(string redirectUri)
        {
            //ClientSecrets client_secrets = new ClientSecrets
            //{
            //    ClientId = client_id,
            //    ClientSecret = client_secret
            //};
            //GoogleAuthorizationCodeFlow credential = new GoogleAuthorizationCodeFlow(new GoogleAuthorizationCodeFlow.Initializer
            //{
            //    ClientSecrets = client_secrets,
            //    Scopes = scopes
            //});
            //AuthorizationCodeRequestUrl url = credential.CreateAuthorizationCodeRequest(redirectUri);

            //return url.Build().ToString();

            // This isn't the best way to do this but security settings on the server keep changing and breaking this method otherwise.
            string tempScope = "https://www.googleapis.com/auth/userinfo.email";
            string url = string.Format("https://accounts.google.com/o/oauth2/v2/auth?client_id={0}&redirect_uri={1}&scope={2}&access_type=offline&response_type=code",
                client_id, redirectUri, tempScope);
            return url;
        }

        [WebMethod]
        public string GetRefreshToken(string authCode, string redirectUri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://accounts.google.com/o/oauth2/token");
            string postData = string.Format(
                "code={0}&client_id={1}&client_secret={2}&redirect_uri={3}&grant_type=authorization_code",
                authCode, client_id, client_secret, redirectUri);
            byte[] data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            try
            {
                using (Stream stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                string responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                XElement responseElement = XElement.Parse(responseString).Element("success");

                return responseElement.Attribute("token").Value;
            }
            catch (WebException e)
            {
                //log.Error(new Common.ExceptionManager().GetText(e));
                throw e;
            }
        }

        [WebMethod]
        public string GetAccessToken(string authCode, string redirectUri)
        {
            string postData = string.Format("code={0}&client_id={1}&client_secret={2}&redirect_uri={3}&grant_type=authorization_code",
                authCode, client_id, client_secret, redirectUri);
            string url = "https://accounts.google.com/o/oauth2/token";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url.ToString());

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            UTF8Encoding utfenc = new UTF8Encoding();
            byte[] bytes = utfenc.GetBytes(postData);
            Stream os = null;

            try
            {
                request.ContentLength = bytes.Length;
                os = request.GetRequestStream();
                os.Write(bytes, 0, bytes.Length);

                HttpWebResponse webResponse = (HttpWebResponse)request.GetResponse();
                Stream responseStream = webResponse.GetResponseStream();
                StreamReader responseStreamReader = new StreamReader(responseStream);

                // Parse token from result.
                return responseStreamReader.ReadToEnd();
            }
            catch (WebException e)
            {
                //log.Error(new Common.ExceptionManager().GetText(e));
                throw e;
            }
        }
    }
}
