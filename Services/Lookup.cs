using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Collections.Generic;

using Common;
using Common.WebServices.Dto;
using WdhServices;
using WdhServices.Services.Dto;
using WdhServices.Services.Data;

/// <summary>
/// Retrieve standard lookup values for WDH
/// </summary>
[WebService(Namespace = "https://dws.wyo.gov/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class Lookup : System.Web.Services.WebService
{

    public Lookup()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    /// <summary>
    /// Gets the state lookup table.
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public List<LuStateDto> GetStates()
    {
        List<LuStateDto> states = new List<LuStateDto>();
        StateLookupData stateLookups = new StateLookupData();

        states = stateLookups.GetLuStateDtoList();

        return states;
        
    }

    [WebMethod]
    public List<LuCountyDto> GetCounties()
    {
        List<LuCountyDto> counties = new List<LuCountyDto>();
        CountyLookupData countyLookups = new CountyLookupData();

        counties = countyLookups.GetLuCountyList();

        return counties;
    }
}

