using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using log4net;

using Common;
using Common.Configuration;
using Common.Security;
using Common.WebServices.Dto;

using WdhServices.Services.Data;
using System.Net;

namespace WdhServices.Services
{
    /// <summary>
    /// Change: MW - 09/30/2011 - Use gmail smtp
    /// </summary>
    public class MessageSys
    {
        public MessageSys()
        {
        }

        private static readonly ILog log = LogManager.GetLogger(typeof(MessageSys));
        private const string SMTP_SERVER_KEY = "Common.Mail.SmtpServer";
        private const string SMTP_SERVER_PORT = "Common.Mail.SmtpPort";
        private const string SMTP_SERVER_USER = "Common.Mail.SmtpUser";
        private const string SMTP_SERVER_PASSWORD = "Common.Mail.SmtpPassword";
        private const string SMTP_SERVER_TIMEOUT = "Common.Mail.SmtpTimeout";

        public int SendMessage(int applicationId, string subject, string body)
        {
            MessageDto messageDto = new MessageDto();

            List<string> emailList = new NotificationSys().GetEmailAddresses(applicationId);

            foreach (string emailAddress in emailList)
                messageDto.To.Add(emailAddress);

            messageDto.Subject = subject;
            messageDto.Body = body;

            return SendMessage(applicationId, messageDto);
        }
        public int SendMessage(int applicationId, MessageDto messageDto)
        {
            return SendMessage(applicationId, messageDto, false);
        }

        /// <summary>
        /// Change: MW - 09/30/2011 - Use gmail SSL & [GSECURE]
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="messageDto"></param>
        /// <param name="noNotification"></param>
        /// <returns></returns>
        public int SendMessage(int applicationId, MessageDto messageDto, bool noNotification)
        {
            //Always BCC the Notification email if the message is not already 
            //adressed to them.
            if (noNotification)
            {
                List<string> emailList = new NotificationSys().GetEmailAddresses(applicationId);

                foreach (string address in emailList)
                {
                    if (!messageDto.To.Contains(address)
                        && !messageDto.Cc.Contains(address)
                        && !messageDto.Bcc.Contains(address))
                    {
                        messageDto.Bcc.Add(address);
                    }
                }
            }

            //If the caller has not set the From for the message we use the default from for the app.
            if (messageDto.From.Length == 0)
            {
                messageDto.From = new ApplicationSys().GetApplication(applicationId).FromNotification.EmailAddress;
            }


            MailMessage mailMessage = new MailMessage();

            mailMessage.From = new MailAddress(messageDto.From);

            AddAddresses(mailMessage.To, messageDto.To);
            AddAddresses(mailMessage.CC, messageDto.Cc);
            AddAddresses(mailMessage.Bcc, messageDto.Bcc);

            mailMessage.IsBodyHtml = messageDto.IsHtmlFormat;

            mailMessage.Subject = FormatText(messageDto.Subject, messageDto.SubjectToken);
            mailMessage.Subject = mailMessage.Subject.StartsWith("[SECURE]") ? mailMessage.Subject.Replace("[SECURE]", "[GSECURE]") : mailMessage.Subject;

            mailMessage.Body = FormatText(messageDto.Body, messageDto.BodyToken);


            foreach (MessageAttachmentDto messageAttachmentDto in messageDto.MessageAttachmentList)
            {
                MemoryStream memoryStream = new MemoryStream(messageAttachmentDto.Content);
                mailMessage.Attachments.Add(new Attachment(memoryStream, messageAttachmentDto.Filename, messageAttachmentDto.MimeType));
            }

            string smtpServer = ConfigurationReader.ReadString(SMTP_SERVER_KEY);
            int smtpPort = ConfigurationReader.ReadInt32(SMTP_SERVER_PORT);
            string smtpUser = ConfigurationReader.ReadString(SMTP_SERVER_USER);
            string smtpPwd = ConfigurationReader.ReadString(SMTP_SERVER_PASSWORD);
            int smtpTimeout = ConfigurationReader.ReadInt32(SMTP_SERVER_TIMEOUT);

#if DEBUG
            SmtpClient smtpClient = new SmtpClient 
            {
                Host = "relay.wyo.gov",
                Port = smtpPort,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(smtpUser, smtpPwd),
                Timeout = smtpTimeout
            };
#else
            SmtpClient smtpClient = new SmtpClient 
            {
                Host = "relay.wyo.gov",
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Timeout = smtpTimeout
            };

#endif


            try
            {
                smtpClient.Send(mailMessage);

                return Insert(messageDto);
            }
            catch (Exception e)
            {
                Exception newException = new NotificationException("Failed to send email.", e);
                log.Error(new ExceptionManager().GetText(newException));
                System.Diagnostics.Debug.WriteLine(new ExceptionManager().GetText(newException));

                throw newException;
            }
            
        }

        private void AddAddresses(MailAddressCollection mailAddressCollection, List<string> emailList)
        {
            foreach (string email in emailList)
                mailAddressCollection.Add(new MailAddress(email));
        }
        private string FormatText(string text, List<string> token)
        {
            string[] tokenArray = new string[token.Count];
            
            token.CopyTo(tokenArray, 0);
            
            string val = string.Format(text, tokenArray);

            return val;
        }

        private int Insert(MessageDto o)
        {
            int messageId = new MessageData().Insert(o);

            foreach (MessageAttachmentDto m in o.MessageAttachmentList)
            {
                m.MessageId = messageId;
                new MessageAttachmentData().Insert(m);
            }

            return messageId;
        }
    }

}
