using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using WdhServices.Services.Dto;
using WdhServices.Services.Data;


namespace WdhServices.Services
{
    public class NodeSys
    {
        NodeData nodeData = new NodeData();

        public NodeSys()
        {
        }

        public int GetApplicationNodeId(int applicationId, string machineName)
        {
            NodeDto nodeDto = GetNode(machineName);
            ApplicationNodeDto applicationNodeDto = new ApplicationNodeSys().GetApplicationNode(applicationId, nodeDto.NodeId);
            return applicationNodeDto.ApplicationNodeId;
        }
        public NodeDto GetNode(string machineName)
        {
            return nodeData.GetNode(machineName);
        }
    }
}

