using System;
using System.Collections.Generic;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;

using log4net;

using Common;
using Common.Security;
using Common.Security.Dto;
using Common.Utilities;
using Common.Web;
using Common.WebServices.Dto;
using Common.Xml;

using WdhServices.Services;
using WdhServices.Services.Dto;

[WebService(Namespace = "https://dws.wyo.gov/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class Request : System.Web.Services.WebService
{
    public Request()
    {
    }

    #region IRequestTrackerService Members

    [WebMethod]
    public void SubmitRequest(HttpRequestDto webRequestTrackerDto)
    {
        if (webRequestTrackerDto != null)
        {
            Logger.Debug("Request.SubmitRequest", webRequestTrackerDto.RequestId.ToString().ToUpper());

            new RequestSys().Insert(webRequestTrackerDto);
        }
    }

    #endregion
            
}


