﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Common.Web;

using WdhServices.Services.Data;
using WdhServices.Services.Dto;


namespace WdhServices.Services
{
    public class RequestSys
    {
        public void Insert(HttpRequestDto httpRequestDto)
        {
            RequestDto requestDto = MapDto(httpRequestDto);
            new RequestData().Insert(requestDto);
        }

        private RequestDto MapDto(HttpRequestDto httpRequestDto)
        {
            RequestDto requestDto = new RequestDto();

            requestDto.ApplicationNodeId = new NodeSys().GetApplicationNodeId(httpRequestDto.ApplicationId, httpRequestDto.MachineName);
            requestDto.CreateDate = httpRequestDto.CreateDate;
            requestDto.Domain = httpRequestDto.Domain;
            requestDto.IPAddress = httpRequestDto.ClientIpAddress;
            requestDto.MachineName = httpRequestDto.MachineName;
            requestDto.Method = httpRequestDto.Method;
            requestDto.Milliseconds = (int)httpRequestDto.Milliseconds;
            requestDto.Path = httpRequestDto.Path;
            requestDto.QueryString = httpRequestDto.QueryString;
            requestDto.RawUrl = httpRequestDto.RawUrl;
            requestDto.Referer = httpRequestDto.Referer;
            requestDto.RequestId = httpRequestDto.RequestId;
            requestDto.Uri = httpRequestDto.Url;
            requestDto.UserAgent = httpRequestDto.UserAgent;
            requestDto.UserName = httpRequestDto.UserName;
            requestDto.Cookies = httpRequestDto.Cookies;

            return requestDto;
        }

    }
}
