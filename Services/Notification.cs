using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;

using log4net;
using Common;
using Common.WebServices.Dto;

using WdhServices.Services;

/// <summary>
/// Summary description for Notification
/// </summary>
[WebService(Namespace = "https://dws.wyo.gov/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class Notification : System.Web.Services.WebService
{
    private static readonly ILog log = LogManager.GetLogger(typeof(Notification));

    public Notification()
    {

    }

    [WebMethod]
    public int SendMessage(int applicationId, MessageDto messageDto)
    {
        log.Debug(string.Format("SendMessage - Application Id: {0} Subject Id: {1}", applicationId, messageDto.Subject));

        try
        {
            return new MessageSys().SendMessage(applicationId, messageDto);
        }
        catch (Exception e)
        {
            log.Error(new ExceptionManager().GetText(e));
            throw e;
        }
    }

    [WebMethod]
    public int SendNotification(int applicationId, string subject, string body)
    {
        log.Debug(string.Format("SendNotification - Application Id: {0} Subject Id: {1}", applicationId, subject));

        try
        {
            int result = new MessageSys().SendMessage(applicationId, subject, body);

            log.Debug(string.Format("Success, Message Id: {0}", result));

            return result;
        }
        catch (Exception e)
        {
            log.Error(new ExceptionManager().GetText(e));
            throw e;
        }
    }

}

