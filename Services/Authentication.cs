﻿using System.IO;
using System.Threading;
using System.Web.Services;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using System.Threading.Tasks;

namespace WdhServices.Services
{
    [WebService(Namespace = "https://dws.wyo.gov/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class Authentication : WebService
    {
        [WebMethod]
        public void GetAccessToken(string authorizationCode)
        {
            var credential = GetAuthService();
        }

        private async Task<Google.Apis.Oauth2.v2.Oauth2Service> GetAuthService()
        {
            UserCredential credential;

            using (var stream = new FileStream("client_secrets.json", FileMode.Open, FileAccess.Read))
            {
                credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    new[] { Google.Apis.Oauth2.v2.Oauth2Service.Scope.UserinfoEmail },
                    "user", CancellationToken.None);
            }

            var service = new Google.Apis.Oauth2.v2.Oauth2Service(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "DWS Gateway"
            });

            return service;
        }
    }
}