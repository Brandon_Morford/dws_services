using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Common.WebServices.Dto;
using Common.Utilities;

/// <summary>
/// Summary description for WdhServicesHttpApplicationBase
/// </summary>
public class WdhServicesHttpApplicationBase : Common.Web.HttpApplicationBase
{
    public WdhServicesHttpApplicationBase()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    protected override void HttpApplication_Error(object sender, EventArgs e)
    {
        WebErrorDto webErrorDto = base.GetWebErrorDto(HttpContext.Current.Server.GetLastError());

        Logger.Debug("WdhServicesHttpApplicationBase.HttpApplication_Error", webErrorDto.ToString());
    }

    protected override void HttpApplication_EndRequest(object sender, EventArgs e)
    {
        //Do not use the base implementation.
    }
}
