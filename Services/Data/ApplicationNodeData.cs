using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

using Common.Cache;
using Common.Data;

using WdhServices.Services.Dto;

namespace WdhServices.Services.Data
{
    public class ApplicationNodeData
    {
        private static readonly string CACHE_KEY_APPLICATION_NODE_DICTIONARY = "ApplicationNodeData.ApplicationNodeDictionary";

        public ApplicationNodeData()
        {
        }

        public ApplicationNodeDto GetApplicationNode(int applicationId, int nodeId)
        {
            Dictionary<string, ApplicationNodeDto> applicationNodeDictionary = GetApplicationNodeDictionary();
            string key = GetKey(applicationId, nodeId);
            if (applicationNodeDictionary.ContainsKey(key))
                return applicationNodeDictionary[key];
            else
            {
                ApplicationNodeDto applicationNodeDto = new ApplicationNodeDto();
                applicationNodeDto.ApplicationId = applicationId;
                applicationNodeDto.NodeId = nodeId;

                Insert(applicationNodeDto);

                applicationNodeDictionary.Add(key, applicationNodeDto);

                return applicationNodeDto;
            }
        }
        public List<ApplicationNodeDto> GetApplicationNodeList()
        {
            List<ApplicationNodeDto> result = new List<ApplicationNodeDto>();

            DbClient dbClient = new DbClient("DWSCommon");

            dbClient.Commands[0].Text = "usp_ApplicationNodesSelectAll";

            using (IDataReader dataReader = dbClient.Commands[0].ExecuteReader())
            {
                while (dataReader.Read())
                {
                    result.Add(ReadRecord(dataReader));
                }
            }

            return result;
        }
        public void Insert(ApplicationNodeDto o)
        {
            DbClient dbClient = new DbClient("DWSCommon");

            dbClient.Commands[0].Text = "usp_ApplicationNodeInsert";
            dbClient.Commands[0].Parameters.Add("@ApplicationId", o.ApplicationId);
            dbClient.Commands[0].Parameters.Add("@NodeId", o.NodeId);

            int index = dbClient.Commands[0].Parameters.Add("@ApplicationNodeId", DbType.Int32, ParameterDirection.Output);

            dbClient.Commands[0].ExecuteNonQuery();

            o.ApplicationNodeId = DbHelper.ReadInt32(dbClient.Commands[0].Parameters[index].Value);
        }

        private Dictionary<string, ApplicationNodeDto> GetApplicationNodeDictionary()
        {
            if (CacheManager.Get(CACHE_KEY_APPLICATION_NODE_DICTIONARY) == null)
            {
                List<ApplicationNodeDto> applicationNodeList = GetApplicationNodeList();
                Dictionary<string, ApplicationNodeDto> applicationNodeDictionary = new Dictionary<string, ApplicationNodeDto>();
                foreach (ApplicationNodeDto applicationNodeDto in applicationNodeList)
                    applicationNodeDictionary.Add(GetKey(applicationNodeDto.ApplicationId, applicationNodeDto.NodeId), applicationNodeDto);

                CacheManager.Set(CACHE_KEY_APPLICATION_NODE_DICTIONARY, applicationNodeDictionary);

                return applicationNodeDictionary;
            }
            else
                return (Dictionary<string, ApplicationNodeDto>)CacheManager.Get(CACHE_KEY_APPLICATION_NODE_DICTIONARY);
        }
        private string GetKey(int applicationId, int nodeId)
        {
            return string.Format("{0}|{1}", applicationId, nodeId);
        }
        private ApplicationNodeDto ReadRecord(IDataReader dataReader)
        {
            ApplicationNodeDto record = new ApplicationNodeDto();

            record.ApplicationNodeId = DbHelper.ReadInt32(dataReader["ApplicationNodeId"]);
            record.ApplicationId = DbHelper.ReadInt32(dataReader["ApplicationId"]);
            record.NodeId = DbHelper.ReadInt32(dataReader["NodeId"]);

            return record;
        }
    }
}
