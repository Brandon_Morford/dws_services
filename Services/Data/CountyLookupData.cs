using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.IO;

using Common.Data;
using Common.Utilities;

using WdhServices.Services.Dto;

namespace WdhServices.Services.Data
{
    /// <summary>
    /// Summary description for CountyLookupData
    /// </summary>
    public class CountyLookupData
    {
        public CountyLookupData()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        /*
        *   Generated (CodeSmith): 5/30/2007 1:10:59 PM
        */


        public List<LuCountyDto> GetLuCountyList()
        {
            List<LuCountyDto> result = new List<LuCountyDto>();

            DbClient dbClient = new DbClient("DWSCommon");

            dbClient.Commands[0].Text = "prLuCountiesSelectAll";

            using (IDataReader dataReader = dbClient.Commands[0].ExecuteReader())
            {
                while (dataReader.Read())
                {
                    result.Add(ReadRecord(dataReader));
                }
            }

            return result;
        }

        private LuCountyDto ReadRecord(IDataReader dataReader)
        {
            LuCountyDto record = new LuCountyDto();

            record.CountyID = DbHelper.ReadInt32(dataReader["CountyID"]);
            record.CountyNumber = DbHelper.ReadString(dataReader["CountyNumber"]);
            record.Abbreviation = DbHelper.ReadString(dataReader["Abbreviation"]);
            record.CountyName = DbHelper.ReadString(dataReader["CountyName"]);
            record.StateNumber = DbHelper.ReadString(dataReader["StateNumber"]);

            return record;
        }


    }
}