using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

using Common.Cache;
using Common.Data;

using WdhServices.Services.Dto;


namespace WdhServices.Services.Data
{
    public class NodeData
    {
        private static readonly string CACHE_KEY_NODE_DICTIONARY_BY_NODE_NAME = "NodeData.NodeDictionaryByNodeName";

        public NodeData()
        {
        }

        public NodeDto GetNode(string nodeName)
        {
            Dictionary<string, NodeDto> nodeDictionary = GetNodeDictionaryByNodeName();
            if (nodeDictionary.ContainsKey(nodeName))
                return nodeDictionary[nodeName];
            else
            {
                NodeDto nodeDto = new NodeDto();
                nodeDto.NodeName = nodeName;

                Insert(nodeDto);

                return nodeDto;
            }
        }
        public void Insert(NodeDto o)
        {
            DbClient dbClient = new DbClient("DWSCommon");

            dbClient.Commands[0].Text = "usp_NodeInsert";
            dbClient.Commands[0].Parameters.Add("@NodeName", o.NodeName);

            int index = dbClient.Commands[0].Parameters.Add("@NodeId", DbType.Int32, ParameterDirection.Output);

            dbClient.Commands[0].ExecuteNonQuery();

            o.NodeId = DbHelper.ReadInt32(dbClient.Commands[0].Parameters[index].Value);
        }

        private List<NodeDto> GetNodeList()
		{
			List<NodeDto> result = new List<NodeDto>();

            DbClient dbClient = new DbClient("DWSCommon");
			
			dbClient.Commands[0].Text = "usp_NodesSelectAll";
			
			using (IDataReader dataReader = dbClient.Commands[0].ExecuteReader())
			{
				while (dataReader.Read())
				{
					result.Add(ReadRecord(dataReader));
				}
			}
			
			return result;
		}
        private Dictionary<string, NodeDto> GetNodeDictionaryByNodeName()
        {
            if (CacheManager.Get(CACHE_KEY_NODE_DICTIONARY_BY_NODE_NAME) == null)
            {
                List<NodeDto> nodeList = GetNodeList();
                Dictionary<string, NodeDto> nodeDictionary = new Dictionary<string, NodeDto>();
                foreach (NodeDto nodeDto in nodeList)
                    nodeDictionary.Add(nodeDto.NodeName, nodeDto);

                CacheManager.Set(CACHE_KEY_NODE_DICTIONARY_BY_NODE_NAME, nodeDictionary);

                return nodeDictionary;
            }
            else
                return (Dictionary<string, NodeDto>)CacheManager.Get(CACHE_KEY_NODE_DICTIONARY_BY_NODE_NAME);
        }
        private NodeDto ReadRecord(IDataReader dataReader)
        {
            NodeDto record = new NodeDto();

            record.NodeId = DbHelper.ReadInt32(dataReader["NodeId"]);
            record.NodeName = DbHelper.ReadString(dataReader["NodeName"]);

            return record;
        }
    }
}

