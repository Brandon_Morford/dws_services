﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Common.Data;

using WdhServices.Services.Dto;

namespace WdhServices.Services.Data
{
    public class RequestData
    {
        public void Insert(RequestDto o)
        {
            DbClient dbClient = new DbClient("DWSCommon");

            dbClient.Commands[0].Text = "prRequestInsert";
            dbClient.Commands[0].Parameters.Add("@RequestId", o.RequestId);
            dbClient.Commands[0].Parameters.Add("@ApplicationNodeId", o.ApplicationNodeId);
            dbClient.Commands[0].Parameters.Add("@CreateDate", o.CreateDate);
            dbClient.Commands[0].Parameters.Add("@RawUrl", o.RawUrl);
            dbClient.Commands[0].Parameters.Add("@Uri", o.Uri);
            dbClient.Commands[0].Parameters.Add("@Domain", o.Domain);
            dbClient.Commands[0].Parameters.Add("@Path", o.Path);
            dbClient.Commands[0].Parameters.Add("@QueryString", o.QueryString);
            dbClient.Commands[0].Parameters.Add("@UserName", o.UserName);
            dbClient.Commands[0].Parameters.Add("@MachineName", o.MachineName);
            dbClient.Commands[0].Parameters.Add("@IPAddress", o.IPAddress);
            dbClient.Commands[0].Parameters.Add("@Referer", o.Referer);
            dbClient.Commands[0].Parameters.Add("@Method", o.Method);
            dbClient.Commands[0].Parameters.Add("@UserAgent", o.UserAgent);
            dbClient.Commands[0].Parameters.Add("@Milliseconds", o.Milliseconds);
            dbClient.Commands[0].Parameters.Add("@Cookies", o.Cookies);

            dbClient.Commands[0].ExecuteNonQuery();
        }

    }
}
