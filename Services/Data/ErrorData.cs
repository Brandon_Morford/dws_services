using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Common.Data;
using Common.Utilities;

using WdhServices.Services.Dto;

namespace WdhServices.Services.Data
{

    public class ErrorData
    {
        public ErrorData()
        {
        }

        public void Insert(ErrorDto o)
        {
            DbClient dbClient = new DbClient("DWSCommon");

            dbClient.Commands[0].Text = "usp_ErrorInsert";
            dbClient.Commands[0].Parameters.Add("@ApplicationNodeId", o.ApplicationNodeId);
            dbClient.Commands[0].Parameters.Add("@RequestId", o.RequestId);
            dbClient.Commands[0].Parameters.Add("@CreateDate", o.CreateDate);
            dbClient.Commands[0].Parameters.Add("@Username", o.Username);
            dbClient.Commands[0].Parameters.Add("@IPAddress", o.IPAddress);
            dbClient.Commands[0].Parameters.Add("@UserAgent", o.UserAgent);
            dbClient.Commands[0].Parameters.Add("@Domain", o.Domain);
            dbClient.Commands[0].Parameters.Add("@Path", o.Path);
            dbClient.Commands[0].Parameters.Add("@QueryString", o.QueryString);
            dbClient.Commands[0].Parameters.Add("@Method", o.Method);
            dbClient.Commands[0].Parameters.Add("@Referer", o.Referer);
            dbClient.Commands[0].Parameters.Add("@Session", o.Session);
            dbClient.Commands[0].Parameters.Add("@Form", o.Form);
            dbClient.Commands[0].Parameters.Add("@Exception", o.Exception);
            dbClient.Commands[0].Parameters.Add("@MessageId", o.MessageId);

            int index = dbClient.Commands[0].Parameters.Add("@ErrorId", DbType.Int32, ParameterDirection.Output);

            dbClient.Commands[0].ExecuteNonQuery();

            o.ErrorId = DbHelper.ReadInt32(dbClient.Commands[0].Parameters[index].Value);
        }

    }

}