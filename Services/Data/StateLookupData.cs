using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Common.Data;
using Common.Utilities;

using WdhServices.Services.Dto;


namespace WdhServices.Services.Data
{
    /// <summary>
    /// Summary description for StateData
    /// </summary>
    public class StateLookupData
    {
        public StateLookupData()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public List<LuStateDto> GetLuStateDtoList()
        {
            List<LuStateDto> result = new List<LuStateDto>();

            DbClient dbClient = new DbClient("DWSCommon");

            dbClient.Commands[0].Text = "prLuStatesSelectAll";

            using (IDataReader dataReader = dbClient.Commands[0].ExecuteReader())
            {
                while (dataReader.Read())
                {
                    result.Add(ReadRecord(dataReader));
                }
            }

            return result;
        }

        private LuStateDto ReadRecord(IDataReader dataReader)
        {
            LuStateDto record = new LuStateDto();

            record.StateID = DbHelper.ReadInt32(dataReader["StateID"]);
            record.Abbreviation = DbHelper.ReadString(dataReader["Abbreviation"]);
            record.StateName = DbHelper.ReadString(dataReader["StateName"]);
            record.StateNumber = DbHelper.ReadString(dataReader["StateNumber"]);

            return record;
        }
    }
}