﻿using System;
using System.Data;

using Common.Data;
using Common.WebServices.Dto;

namespace WdhServices.Services.Data
{
    public class MessageAttachmentData
    {
        public void Insert(MessageAttachmentDto dto)
        {
            DbClient dbClient = new DbClient("DWSCommon");

            int commandIndex = dbClient.Commands.Count;

            dbClient.Commands[commandIndex].Text = "prMessageAttachmentInsertGen";
            dbClient.Commands[commandIndex].Parameters.Add("@MessageId", dto.MessageId);
            dbClient.Commands[commandIndex].Parameters.Add("@Filename", dto.Filename);
            dbClient.Commands[commandIndex].Parameters.Add("@MimeType", dto.MimeType);
            dbClient.Commands[commandIndex].Parameters.Add("@Content", dto.Content);

            int index = dbClient.Commands[commandIndex].Parameters.Add("@MessageAttachmentId", DbType.Int32, ParameterDirection.Output);

            dbClient.Commands[commandIndex].ExecuteNonQuery();

            dto.MessageAttachmentId = DbHelper.ReadInt32(dbClient.Commands[commandIndex].Parameters[index].Value);
        }

    }
}
