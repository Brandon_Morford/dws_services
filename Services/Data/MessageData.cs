using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

using Common.Data;
using Common.Utilities;
using Common.WebServices.Dto;

namespace WdhServices.Services.Data
{

    public class MessageData
    {
        public MessageData()
        {
        }

        public int Insert(MessageDto o)
        {
            DbClient dbClient = new DbClient("DWSCommon");

            dbClient.Commands[0].Text = "usp_MessageInsert";
            dbClient.Commands[0].Parameters.Add("@From", o.From);
            dbClient.Commands[0].Parameters.Add("@To", StringUtil.DelimitList(o.To, ';'));
            dbClient.Commands[0].Parameters.Add("@Cc", StringUtil.DelimitList(o.Cc, ';'));
            dbClient.Commands[0].Parameters.Add("@Bcc", StringUtil.DelimitList(o.Bcc, ';'));
            dbClient.Commands[0].Parameters.Add("@IsHtmlFormat", o.IsHtmlFormat);
            dbClient.Commands[0].Parameters.Add("@Subject", o.Subject);
            dbClient.Commands[0].Parameters.Add("@Body", o.Body);

            int index = dbClient.Commands[0].Parameters.Add("@MessageId", DbType.Int32, ParameterDirection.Output);

            dbClient.Commands[0].ExecuteNonQuery();

            return DbHelper.ReadInt32(dbClient.Commands[0].Parameters[index].Value);
        }

    }

}