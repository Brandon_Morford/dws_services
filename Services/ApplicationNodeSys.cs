using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using WdhServices.Services.Dto;
using WdhServices.Services.Data;

namespace WdhServices.Services
{

    public class ApplicationNodeSys
    {
        ApplicationNodeData applicationNodeData = new ApplicationNodeData();

        public ApplicationNodeSys()
        {
        }

        public ApplicationNodeDto GetApplicationNode(int applicationId, int nodeId)
        {
            return applicationNodeData.GetApplicationNode(applicationId, nodeId);
        }
    }

}
