using System;
using System.Collections.Generic;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using Common.Xml;

using log4net;

using Common;
using Common.Security;
using Common.Security.Dto;
using Common.WebServices.Dto;

using WdhServices.Services;
using WdhServices.Services.Dto;


[WebService(Namespace = "https://dws.wyo.gov/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class Error : System.Web.Services.WebService
{
    private static readonly ILog log = LogManager.GetLogger(typeof(Error));

    public Error()
    {
    }
    
    #region IErrorService Members

    [WebMethod]
    public int SubmitError(WebErrorDto webErrorDto)
    {
        //TODO, record information about the request.
        log.Debug(XmlSerializer.Serialize(webErrorDto, typeof(WebErrorDto)));

        try
        {
            ApplicationDto applicationDto = new ApplicationSys().GetApplication(webErrorDto.ApplicationId);

            ErrorDto errorDto = MapDto(webErrorDto, applicationDto);
            errorDto.MessageId = SendMessage(webErrorDto, applicationDto, webErrorDto.MachineName);

            new ErrorSys().Save(errorDto);

            return errorDto.ErrorId;
        }
        catch (Exception e)
        {
            log.Error(new ExceptionManager().GetText(e));
            throw e;
        }
    }

    #endregion

    private ErrorDto MapDto(WebErrorDto webErrorDto, ApplicationDto applicationDto)
    {
        ErrorDto errorDto = new ErrorDto();

        errorDto.ApplicationNodeId = new NodeSys().GetApplicationNodeId(applicationDto.ApplicationId, webErrorDto.MachineName);
        errorDto.CreateDate = webErrorDto.CreateDate;
        errorDto.Domain = webErrorDto.Domain;
        errorDto.Exception = webErrorDto.Exception;
        errorDto.Form = webErrorDto.Form;
        errorDto.IPAddress = webErrorDto.IPAddress;
        errorDto.Method = webErrorDto.Method;
        errorDto.Path = webErrorDto.Path;
        errorDto.QueryString = webErrorDto.QueryString;
        errorDto.Referer = webErrorDto.Referer;
        errorDto.RequestId = webErrorDto.RequestId;
        errorDto.Session = webErrorDto.Session;
        errorDto.UserAgent = webErrorDto.UserAgent;
        errorDto.Username = webErrorDto.Username;

        return errorDto;
    }
    private Nullable<int> SendMessage(WebErrorDto errorDto, ApplicationDto applicationDto, string machineName)
    {
        Nullable<int> returnValue = new Nullable<int>();

        List<string> emailAddressList = new NotificationSys().GetEmailAddresses(applicationDto.ApplicationId);

        MessageDto messageDto = new MessageDto();
        messageDto.From = applicationDto.FromNotification.EmailAddress;

        foreach (string emailAddress in emailAddressList)
        {
            if (!messageDto.To.Contains(emailAddress))
                messageDto.To.Add(emailAddress);
        }

        messageDto.Subject = string.Format("Error in {0} ({1})", applicationDto.Name, machineName);
        messageDto.Body = errorDto.ToString();

        try
        {
            returnValue = new MessageSys().SendMessage(applicationDto.ApplicationId, messageDto);
        }
        catch (Exception e)
        {
            log.Error(new ExceptionManager().GetText(e));
        }

        return returnValue;
    }
}

