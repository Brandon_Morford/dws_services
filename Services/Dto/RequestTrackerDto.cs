using System;
using System.IO;

/*
*   Generated (CodeSmith): 
*/

namespace WdhServices.Services.Dto
{
    [Serializable()]
    public partial class RequestTrackerDto
    {
        private int requestRequestId;
        private Nullable<int> applicationId;
        private Guid requestId;
        private Nullable<DateTime> createDate;
        private string rawUrl;
        private string uRl;
        private string domain;
        private string path;
        private string queryString;
        private string userName;
        private string machineName;
        private string iPAddress;
        private string referer;
        private string method;
        private string userAgent;
        private Nullable<int> milliseconds;

        public RequestTrackerDto()
        {
        }

        public int RequestRequestId
        {
            get { return requestRequestId; }
            set { requestRequestId = value; }
        }
        public Nullable<int> ApplicationId
        {
            get { return applicationId; }
            set { applicationId = value; }
        }
        public Guid RequestId
        {
            get { return requestId; }
            set { requestId = value; }
        }
        public Nullable<DateTime> CreateDate
        {
            get { return createDate; }
            set { createDate = value; }
        }
        public string RawUrl
        {
            get { return rawUrl; }
            set { rawUrl = value; }
        }
        public string URl
        {
            get { return uRl; }
            set { uRl = value; }
        }
        public string Domain
        {
            get { return domain; }
            set { domain = value; }
        }
        public string Path
        {
            get { return path; }
            set { path = value; }
        }
        public string QueryString
        {
            get { return queryString; }
            set { queryString = value; }
        }
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }
        public string MachineName
        {
            get { return machineName; }
            set { machineName = value; }
        }
        public string IPAddress
        {
            get { return iPAddress; }
            set { iPAddress = value; }
        }
        public string Referer
        {
            get { return referer; }
            set { referer = value; }
        }
        public string Method
        {
            get { return method; }
            set { method = value; }
        }
        public string UserAgent
        {
            get { return userAgent; }
            set { userAgent = value; }
        }
        public Nullable<int> Milliseconds
        {
            get { return milliseconds; }
            set { milliseconds = value; }
        }
    }
}


