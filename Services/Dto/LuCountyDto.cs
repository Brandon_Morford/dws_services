using System;
using System.IO;

/*
*   Generated (CodeSmith): 5/30/2007 1:07:06 PM
*/

namespace WdhServices.Services.Dto
{
    [Serializable()]
    public partial class LuCountyDto
    {
        private int countyID;
        private string countyNumber;
        private string abbreviation;
        private string countyName;
        private string stateNumber;

        public LuCountyDto()
        {
        }

        public int CountyID
        {
            get { return countyID; }
            set { countyID = value; }
        }
        public string CountyNumber
        {
            get { return countyNumber; }
            set { countyNumber = value; }
        }
        public string Abbreviation
        {
            get { return abbreviation; }
            set { abbreviation = value; }
        }
        public string CountyName
        {
            get { return countyName; }
            set { countyName = value; }
        }
        public string StateNumber
        {
            get { return stateNumber; }
            set { stateNumber = value; }
        }
    }
}

