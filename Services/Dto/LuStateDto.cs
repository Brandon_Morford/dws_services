using System;
using System.IO;

/*
*   Generated (CodeSmith): 5/30/2007 11:13:29 AM
*/

namespace WdhServices.Services.Dto
{
    [Serializable()]
    public partial class LuStateDto
    {
        private int stateID;
        private string abbreviation;
        private string stateName;
        private string stateNumber;

        public LuStateDto()
        {
        }

        public int StateID
        {
            get { return stateID; }
            set { stateID = value; }
        }
        public string Abbreviation
        {
            get { return abbreviation; }
            set { abbreviation = value; }
        }
        public string StateName
        {
            get { return stateName; }
            set { stateName = value; }
        }
        public string StateNumber
        {
            get { return stateNumber; }
            set { stateNumber = value; }
        }
    }
}

