using System;
using System.IO;

/*
*   Generated (CodeSmith): 1/11/2007 1:42:19 PM
*/

namespace WdhServices.Services.Dto
{
    [Serializable()]
    public partial class ApplicationNodeDto
    {
        private int applicationNodeId;
        private int applicationId;
        private int nodeId;

        public ApplicationNodeDto()
        {
        }

        public int ApplicationNodeId
        {
            get { return applicationNodeId; }
            set { applicationNodeId = value; }
        }
        public int ApplicationId
        {
            get { return applicationId; }
            set { applicationId = value; }
        }
        public int NodeId
        {
            get { return nodeId; }
            set { nodeId = value; }
        }
    }
}

