﻿using System;
using System.IO;

namespace WdhServices.Services.Dto
{
    [Serializable()]
    public partial class RequestDto
    {
        private Guid requestId;
        private int applicationNodeId;
        private DateTime createDate;
        private string rawUrl;
        private string uri;
        private string domain;
        private string path;
        private string queryString;
        private string userName;
        private string machineName;
        private string iPAddress;
        private string referer;
        private string method;
        private string userAgent;
        private int milliseconds;
        private string cookies;

        public RequestDto()
        {
        }

        public Guid RequestId
        {
            get { return requestId; }
            set { requestId = value; }
        }
        public int ApplicationNodeId
        {
            get { return applicationNodeId; }
            set { applicationNodeId = value; }
        }
        public DateTime CreateDate
        {
            get { return createDate; }
            set { createDate = value; }
        }
        public string RawUrl
        {
            get { return rawUrl; }
            set { rawUrl = value; }
        }
        public string Uri
        {
            get { return uri; }
            set { uri = value; }
        }
        public string Domain
        {
            get { return domain; }
            set { domain = value; }
        }
        public string Path
        {
            get { return path; }
            set { path = value; }
        }
        public string QueryString
        {
            get { return queryString; }
            set { queryString = value; }
        }
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }
        public string MachineName
        {
            get { return machineName; }
            set { machineName = value; }
        }
        public string IPAddress
        {
            get { return iPAddress; }
            set { iPAddress = value; }
        }
        public string Referer
        {
            get { return referer; }
            set { referer = value; }
        }
        public string Method
        {
            get { return method; }
            set { method = value; }
        }
        public string UserAgent
        {
            get { return userAgent; }
            set { userAgent = value; }
        }
        public int Milliseconds
        {
            get { return milliseconds; }
            set { milliseconds = value; }
        }
        public string Cookies
        {
            get { return cookies; }
            set { cookies = value; }
        }
    }
}

