using System;
using System.IO;

/*
*   Generated (CodeSmith): 1/11/2007 2:12:21 PM
*/

namespace WdhServices.Services.Dto
{
    [Serializable()]
    public partial class ErrorDto
    {
        private int errorId;
        private int applicationNodeId;
        private Guid requestId;
        private DateTime createDate;
        private string username;
        private string ipAddress;
        private string userAgent;
        private string domain;
        private string path;
        private string queryString;
        private string method;
        private string referer;
        private string session;
        private string form;
        private string exception;
        private Nullable<int> messageId;

        public ErrorDto()
        {
        }

        public int ErrorId
        {
            get { return errorId; }
            set { errorId = value; }
        }
        public int ApplicationNodeId
        {
            get { return applicationNodeId; }
            set { applicationNodeId = value; }
        }
        public Guid RequestId
        {
            get { return requestId; }
            set { requestId = value; }
        }
        public DateTime CreateDate
        {
            get { return createDate; }
            set { createDate = value; }
        }
        public string Username
        {
            get { return username; }
            set { username = value; }
        }
        public string IPAddress
        {
            get { return ipAddress; }
            set { ipAddress = value; }
        }
        public string UserAgent
        {
            get { return userAgent; }
            set { userAgent = value; }
        }
        public string Domain
        {
            get { return domain; }
            set { domain = value; }
        }
        public string Path
        {
            get { return path; }
            set { path = value; }
        }
        public string QueryString
        {
            get { return queryString; }
            set { queryString = value; }
        }
        public string Method
        {
            get { return method; }
            set { method = value; }
        }
        public string Referer
        {
            get { return referer; }
            set { referer = value; }
        }
        public string Session
        {
            get { return session; }
            set { session = value; }
        }
        public string Form
        {
            get { return form; }
            set { form = value; }
        }
        public string Exception
        {
            get { return exception; }
            set { exception = value; }
        }
        public Nullable<int> MessageId
        {
            get { return messageId; }
            set { messageId = value; }
        }
    }
}

