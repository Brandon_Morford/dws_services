using System;
using System.IO;

/*
*   Generated (CodeSmith): 1/11/2007 12:16:44 PM
*/

namespace WdhServices.Services.Dto
{
    [Serializable()]
    public partial class NodeDto
    {
        private int nodeId;
        private string nodeName;

        public NodeDto()
        {
        }

        public int NodeId
        {
            get { return nodeId; }
            set { nodeId = value; }
        }
        public string NodeName
        {
            get { return nodeName; }
            set { nodeName = value; }
        }
    }
}

